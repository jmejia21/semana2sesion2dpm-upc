package com.joseluis.semana2sesion2;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by JoseLuis on 1/11/17.
 */

public class Adaptador extends RecyclerView.Adapter<viewHolder> {

    List<Menu> lstMenu;

    public Adaptador(List<Menu> lstMenu) {
        this.lstMenu=lstMenu;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout,parent,false);

        return new viewHolder(vista );
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {

        holder.titulo.setText(lstMenu.get(position).getTitulo());
        holder.descripcion.setText(lstMenu.get(position).getDescripcion());
        holder.image.setImageResource(lstMenu.get(position).getImagen() );

    }

    @Override
    public int getItemCount() {
        return lstMenu.size() ;
    }
}
