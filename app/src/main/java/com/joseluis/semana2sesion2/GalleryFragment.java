package com.joseluis.semana2sesion2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


public class GalleryFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gallery, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<Menu> lstMenu = new ArrayList<Menu>();
        lstMenu.add(new Menu(R.drawable.consultoria,"Servicios de Consultoria","Ofrecemos un servicio profesional de asesoría y gestión de proyectos web al servicio de las empresas con la finalidad de ayudarlas a aumentar su presencia en internet y a incrementar su volumen de negocio."));
        lstMenu.add(new Menu(R.drawable.android,"Capacitación en programación Android","Aprende a desarrollar APPS para Android. Construye aplicaciones para Tablets, Smartphones o GPS y distribúyelas a través de Google Play."));
        lstMenu.add(new Menu(R.drawable.ios,"Capacitación en programación IOS","Desarrolla apps para iOS. Súmate el mundo del desarrollo Mobile. Construye aplicaciones para iPhone, iPad e iPod Touch y distribúyelas a través de AppStore"));


        RecyclerView contenedor = (RecyclerView) getActivity().findViewById(R.id.recycler);
        contenedor.setHasFixedSize(true);

        LinearLayoutManager layout = new LinearLayoutManager(getActivity().getApplicationContext());
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        contenedor.setAdapter(new Adaptador(lstMenu));
        contenedor.setLayoutManager(layout);


    }
}