package com.joseluis.semana2sesion2;

/**
 * Created by JoseLuis on 1/11/17.
 */

public class Menu {

    private int imagen;
    private String titulo;
    private String descripcion;

    public Menu(int imagen, String titulo, String descripcion) {
        this.imagen = imagen;
        this.titulo = titulo;
        this.descripcion = descripcion;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getImagen() {
        return imagen;
    }
}
