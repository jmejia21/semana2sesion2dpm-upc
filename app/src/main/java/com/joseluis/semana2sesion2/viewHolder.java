package com.joseluis.semana2sesion2;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by JoseLuis on 1/11/17.
 */

public class viewHolder extends RecyclerView.ViewHolder {

    ImageView image;
    TextView titulo;
    TextView descripcion;




    public viewHolder(View itemView){
        super(itemView);

        titulo = (TextView) itemView.findViewById(R.id.titulo);
        descripcion = (TextView) itemView.findViewById(R.id.descripcion);
        image = (ImageView) itemView.findViewById(R.id.imagen);

    }


}
